# Docker images for building juCi++

These images are used for continuous integration of juCi++ in GitLab CI [jucipp](https://gitlab.com/cppit/jucipp)/[pipelines](https://gitlab.com/cppit/jucipp/pipelines)


## Development

```bash
docker build --tag cppit/jucipp:<distro> <distro>

# e.g.
docker build --tag cppit/jucipp:debian debian
```

